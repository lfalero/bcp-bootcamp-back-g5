# DOCKER #

```console
$ docker build . --tag lfalero/bcp-bootcamp-azure:1.0.0
$ docker run --rm -p 9999:9999 --name bcp-bootcamp-azure lfalero/bcp-bootcamp-azure:1.0.0
```

# KUBERNETES #

```console
$ kubectl create namespace bootcamp
$ kubectl get namespace
$ kubectl apply -f deployment.yaml --record=true
$ kubectl apply -f service.yaml
$ kubectl -n bootcamp get deployment bcp-bootcamp-azure-v1
$ kubectl -n bootcamp get pod
$ kubectl autoscale -n bootcamp deployment bcp-bootcamp-azure-v1 --max 3 --min 1 --cpu-percent 50
$ kubectl -n bootcamp get hpa bcp-bootcamp-azure-v1
```
