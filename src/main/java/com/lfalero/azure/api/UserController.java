
package com.lfalero.azure.api;

import com.lfalero.azure.core.dto.cosmo.UserDto;
import com.lfalero.azure.core.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/user")
@AllArgsConstructor
public class UserController extends BaseController{

    private final UserService userService;

    @GetMapping("/{id}")
    public Mono<UserDto> getUser(@PathVariable String id) {
        return userService.getUser(id);
    }
}
