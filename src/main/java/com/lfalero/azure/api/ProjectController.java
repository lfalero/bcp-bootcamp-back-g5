
package com.lfalero.azure.api;

import com.lfalero.azure.core.dto.cosmo.ProjectDto;
import com.lfalero.azure.core.service.ProjectService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/project")
@AllArgsConstructor
public class ProjectController extends BaseController{

    private final ProjectService projectService;

    @GetMapping
    public Flux<ProjectDto> getProjects() {
        return projectService.getProjects();
    }

    @GetMapping("/{id}")
    public Mono<ProjectDto> getProject(@PathVariable String id) {
        return projectService.getProject(id);
    }
}
