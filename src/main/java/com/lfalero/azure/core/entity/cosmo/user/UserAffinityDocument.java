package com.lfalero.azure.core.entity.cosmo.user;

import lombok.Data;

@Data
public class UserAffinityDocument {

    private String sex;
    private Integer maximumAge;
    private Integer minimumAge;
}
