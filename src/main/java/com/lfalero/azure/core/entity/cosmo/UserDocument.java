package com.lfalero.azure.core.entity.cosmo;

import com.azure.spring.data.cosmos.core.mapping.Container;
import com.azure.spring.data.cosmos.core.mapping.GeneratedValue;
import com.azure.spring.data.cosmos.core.mapping.PartitionKey;
import com.lfalero.azure.core.dto.cosmo.user.UserAddressDto;
import com.lfalero.azure.core.dto.cosmo.user.UserAffinityDto;
import com.lfalero.azure.core.dto.cosmo.user.UserFilterDto;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
@Container(containerName = "user")
public class UserDocument {

    @Id
    @GeneratedValue
    private String id;

    @PartitionKey
    private String dni;

    private String firstName;
    private String lastName;
    private String sex;
    private Integer age;
    private boolean roomie;
    private UserAddressDto address;
    private UserAffinityDto affinity;
    private UserFilterDto filter;
}
