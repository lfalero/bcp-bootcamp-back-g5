package com.lfalero.azure.core.entity.cosmo.project;

import lombok.Data;

@Data
public class ProjectAffinityDocument {

    private String sex;
    private Integer maximumAge;
    private Integer minimumAge;
}
