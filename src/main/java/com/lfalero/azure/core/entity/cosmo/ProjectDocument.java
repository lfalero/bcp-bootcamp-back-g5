package com.lfalero.azure.core.entity.cosmo;

import com.azure.spring.data.cosmos.core.mapping.Container;
import com.azure.spring.data.cosmos.core.mapping.GeneratedValue;
import com.azure.spring.data.cosmos.core.mapping.PartitionKey;
import com.lfalero.azure.core.dto.cosmo.project.ProjectAddressDto;
import com.lfalero.azure.core.dto.cosmo.project.ProjectUserDto;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
@Container(containerName = "project")
public class ProjectDocument {

    @Id
    @GeneratedValue
    private String id;

    @PartitionKey
    private String name;

    private String register;
    private ProjectAddressDto address;
    private String description;
    private Integer bedroom;
    private Integer bathroom;
    private String latitude;
    private String longitude;
    private String qr;
    private boolean roomie;
    private List<String> images;
    private Integer ranking;
    private List<ProjectUserDto> usersInterested;
}
