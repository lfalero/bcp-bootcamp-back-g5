package com.lfalero.azure.core.entity.cosmo.user;

import lombok.Data;

@Data
public class UserAddressDocument {

    private Integer number;
    private String street;
    private String district;
    private String department;
}
