package com.lfalero.azure.core.entity.cosmo.user;

import lombok.Data;

@Data
public class UserFilterDocument {

    private boolean reniec;
    private boolean judicialRecord;
    private boolean sentinel;
}
