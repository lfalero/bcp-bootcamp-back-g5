package com.lfalero.azure.core.entity.cosmo.project;

import lombok.Data;

@Data
public class ProjectUserDocument {

    private String id;
    private String dni;
    private String firstName;
    private String lastName;
    private String sex;
    private Integer age;
    private boolean roomie;
    private ProjectAddressDocument address;
    private ProjectAffinityDocument affinity;
    private ProjectFilterDocument filter;
}
