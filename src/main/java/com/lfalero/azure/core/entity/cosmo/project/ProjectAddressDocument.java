package com.lfalero.azure.core.entity.cosmo.project;

import lombok.Data;

@Data
public class ProjectAddressDocument {

    private Integer number;
    private String street;
    private String district;
    private String department;
}
