package com.lfalero.azure.core.entity.cosmo.project;

import lombok.Data;

@Data
public class ProjectFilterDocument {

    private boolean reniec;
    private boolean judicialRecord;
    private boolean sentinel;
}
