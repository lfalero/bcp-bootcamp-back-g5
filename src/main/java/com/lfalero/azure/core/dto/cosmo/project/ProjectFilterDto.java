package com.lfalero.azure.core.dto.cosmo.project;

import lombok.Data;

@Data
public class ProjectFilterDto {

    private boolean reniec;
    private boolean judicialRecord;
    private boolean sentinel;
}
