package com.lfalero.azure.core.dto.cosmo;

import com.lfalero.azure.core.dto.cosmo.user.UserAddressDto;
import com.lfalero.azure.core.dto.cosmo.user.UserAffinityDto;
import com.lfalero.azure.core.dto.cosmo.user.UserFilterDto;
import lombok.Data;

@Data
public class UserDto {

    private String id;
    private String dni;
    private String firstName;
    private String lastName;
    private String sex;
    private Integer age;
    private boolean roomie;
    private UserAddressDto address;
    private UserAffinityDto affinity;
    private UserFilterDto filter;
}
