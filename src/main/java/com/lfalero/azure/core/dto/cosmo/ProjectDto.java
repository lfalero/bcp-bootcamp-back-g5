package com.lfalero.azure.core.dto.cosmo;

import com.lfalero.azure.core.dto.cosmo.project.ProjectAddressDto;
import com.lfalero.azure.core.dto.cosmo.project.ProjectUserDto;
import lombok.Data;

import java.util.List;

@Data
public class ProjectDto {

    private String id;
    private String name;
    private String register;
    private ProjectAddressDto address;
    private String description;
    private Integer bedroom;
    private Integer bathroom;
    private String latitude;
    private String longitude;
    private String qr;
    private boolean roomie;
    private List<String> images;
    private Integer ranking;
    private List<ProjectUserDto> usersInterested;
}
