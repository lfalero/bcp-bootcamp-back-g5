package com.lfalero.azure.core.dto.cosmo.user;

import lombok.Data;

@Data
public class UserFilterDto {

    private boolean reniec;
    private boolean judicialRecord;
    private boolean sentinel;
}
