package com.lfalero.azure.core.dto.cosmo.project;

import lombok.Data;

@Data
public class ProjectUserDto {

    private String id;
    private String dni;
    private String firstName;
    private String lastName;
    private String sex;
    private Integer age;
    private boolean roomie;
    private ProjectAddressDto address;
    private ProjectAffinityDto affinity;
    private ProjectFilterDto filter;
}
