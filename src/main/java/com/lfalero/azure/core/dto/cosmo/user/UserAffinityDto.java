package com.lfalero.azure.core.dto.cosmo.user;

import lombok.Data;

@Data
public class UserAffinityDto {

    private String sex;
    private Integer maximumAge;
    private Integer minimumAge;
}
