package com.lfalero.azure.core.dto.cosmo.project;

import lombok.Data;

@Data
public class ProjectAffinityDto {

    private String sex;
    private Integer maximumAge;
    private Integer minimumAge;
}
