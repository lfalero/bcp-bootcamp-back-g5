package com.lfalero.azure.core.dto.cosmo.user;

import lombok.Data;

@Data
public class UserAddressDto {

    private Integer number;
    private String street;
    private String department;
    private String district;
}
