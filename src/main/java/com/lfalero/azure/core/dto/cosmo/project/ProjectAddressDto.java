package com.lfalero.azure.core.dto.cosmo.project;

import lombok.Data;

@Data
public class ProjectAddressDto {

    private Integer number;
    private String street;
    private String department;
    private String district;
}
