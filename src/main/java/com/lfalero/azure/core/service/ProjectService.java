package com.lfalero.azure.core.service;

import com.lfalero.azure.core.dto.cosmo.ProjectDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProjectService {

    Flux<ProjectDto> getProjects();
    Mono<ProjectDto> getProject(String id);
}
