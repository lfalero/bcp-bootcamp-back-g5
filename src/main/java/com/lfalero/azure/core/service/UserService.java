package com.lfalero.azure.core.service;

import com.lfalero.azure.core.dto.cosmo.UserDto;
import reactor.core.publisher.Mono;

public interface UserService {

    Mono<UserDto> getUser(String id);
}
