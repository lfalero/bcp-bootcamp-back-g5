package com.lfalero.azure.core.service.impl;

import com.lfalero.azure.core.dto.cosmo.project.ProjectAddressDto;
import com.lfalero.azure.core.dto.cosmo.project.ProjectAffinityDto;
import com.lfalero.azure.core.dto.cosmo.project.ProjectFilterDto;
import com.lfalero.azure.core.dto.cosmo.ProjectDto;
import com.lfalero.azure.core.dto.cosmo.project.ProjectUserDto;
import com.lfalero.azure.core.exception.ProjectNotFoundException;
import com.lfalero.azure.core.repository.cosmo.ProjectRepository;
import com.lfalero.azure.core.service.ProjectService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@AllArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    @Override
    public Flux<ProjectDto> getProjects() {
        Flux<ProjectDto> response = projectRepository.findAll()
            .flatMap(element -> {
                ProjectDto projectDto = new ProjectDto();
                BeanUtils.copyProperties(element, projectDto);

                List<ProjectUserDto> usersDto = new ArrayList<>();

                element.getUsersInterested().stream().forEach(elementUser -> {
                    ProjectUserDto userDto = new ProjectUserDto();

                    if(Objects.nonNull(elementUser.getAddress())) {
                        ProjectAddressDto addressDto = new ProjectAddressDto();
                        BeanUtils.copyProperties(elementUser.getAddress(), addressDto);
                        userDto.setAddress(addressDto);
                    }

                    if(Objects.nonNull(elementUser.getAffinity())) {
                        ProjectAffinityDto affinityDto = new ProjectAffinityDto();
                        BeanUtils.copyProperties(elementUser.getAddress(), affinityDto);
                        userDto.setAffinity(affinityDto);
                    }

                    ProjectFilterDto filterDto = new ProjectFilterDto();
                    filterDto.setReniec(true);
                    filterDto.setSentinel(true);
                    filterDto.setJudicialRecord(true);
                    userDto.setFilter(filterDto);

                    usersDto.add(userDto);
                });

                return Mono.just(projectDto);
            });
        response.subscribe(element -> log.info(element.toString()));
        return response;
    }

    @Override
    public Mono<ProjectDto> getProject(String id) {
        Mono<ProjectDto> response = projectRepository.findById(id)
            .switchIfEmpty(Mono.error(new ProjectNotFoundException(id)))
            .flatMap(element -> {
                ProjectDto projectDto = new ProjectDto();
                BeanUtils.copyProperties(element, projectDto);

                List<ProjectUserDto> usersDto = new ArrayList<>();

                element.getUsersInterested().stream().forEach(elementUser -> {
                    ProjectUserDto userDto = new ProjectUserDto();

                    if (Objects.nonNull(elementUser.getAddress())) {
                        ProjectAddressDto addressDto = new ProjectAddressDto();
                        BeanUtils.copyProperties(elementUser.getAddress(), addressDto);
                        userDto.setAddress(addressDto);
                    }

                    if(Objects.nonNull(elementUser.getAffinity())) {
                        ProjectAffinityDto affinityDto = new ProjectAffinityDto();
                        BeanUtils.copyProperties(elementUser.getAddress(), affinityDto);
                        userDto.setAffinity(affinityDto);
                    }

                    ProjectFilterDto filterDto = new ProjectFilterDto();
                    filterDto.setReniec(true);
                    filterDto.setSentinel(true);
                    filterDto.setJudicialRecord(true);
                    userDto.setFilter(filterDto);

                    usersDto.add(userDto);
                });

                return Mono.just(projectDto);
            });
        response.subscribe(element -> log.info(element.toString()));
        return response;
    }
}
