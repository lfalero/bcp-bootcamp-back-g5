package com.lfalero.azure.core.service.impl;

import com.lfalero.azure.core.dto.cosmo.UserDto;
import com.lfalero.azure.core.dto.cosmo.user.UserAddressDto;
import com.lfalero.azure.core.dto.cosmo.user.UserAffinityDto;
import com.lfalero.azure.core.dto.cosmo.user.UserFilterDto;
import com.lfalero.azure.core.exception.UserNotFoundException;
import com.lfalero.azure.core.repository.cosmo.UserRepository;
import com.lfalero.azure.core.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Service
@Slf4j
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public Mono<UserDto> getUser(String id) {
        Mono<UserDto> response = userRepository.findById(id)
            .switchIfEmpty(Mono.error(new UserNotFoundException(id)))
            .flatMap(element -> {

                UserDto userDto = new UserDto();
                BeanUtils.copyProperties(element, userDto);

                if (Objects.nonNull(element.getAddress())) {
                    UserAddressDto addressDto = new UserAddressDto();
                    BeanUtils.copyProperties(element.getAddress(), addressDto);
                    userDto.setAddress(addressDto);
                }

                if (Objects.nonNull(element.getAffinity())) {
                    UserAffinityDto affinityDto = new UserAffinityDto();
                    BeanUtils.copyProperties(element.getAffinity(), affinityDto);
                    userDto.setAffinity(affinityDto);
                }

                UserFilterDto filterDto = new UserFilterDto();
                filterDto.setReniec(true);
                filterDto.setSentinel(true);
                filterDto.setJudicialRecord(true);
                userDto.setFilter(filterDto);

                return Mono.just(userDto);
            });
        response.subscribe(element -> log.info(element.toString()));
        return response;
    }
}
