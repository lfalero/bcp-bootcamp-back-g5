package com.lfalero.azure.core.exception;

import java.text.MessageFormat;

public class ProjectNotFoundException extends RuntimeException{

    public ProjectNotFoundException(String id) {
        super(MessageFormat.format("Proyecto con id:{0} no encontrado", id));
    }
}
