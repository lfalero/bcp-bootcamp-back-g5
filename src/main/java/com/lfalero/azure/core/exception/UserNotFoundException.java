package com.lfalero.azure.core.exception;

import java.text.MessageFormat;

public class UserNotFoundException extends RuntimeException{

    public UserNotFoundException(String id) {
        super(MessageFormat.format("Usuario con id:{0} no encontrado", id));
    }
}
