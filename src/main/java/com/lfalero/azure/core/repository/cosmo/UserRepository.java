package com.lfalero.azure.core.repository.cosmo;

import com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository;
import com.lfalero.azure.core.entity.cosmo.UserDocument;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends ReactiveCosmosRepository<UserDocument, String> {
}
